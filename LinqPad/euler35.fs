#light

// stupid factorial:
let rec fact n = 
    match n with
    | 0 -> 1
    | _ -> n * fact (n - 1)

let isPrime n = 
    match n with
    | x when x < 2 -> false
    | _ -> let bound = int (sqrt(float n))
           seq{2..bound}
           |> Seq.exists (fun x -> n % x = 0) 
           |> not

let breakDownInt n =
    n
    |> string
    |> Seq.map (fun x -> x |> string |> int) 
    |> Seq.toList

let rec intFromList s = 
        match s with 
        | [] -> 0
        | x :: xs -> x * (pown 10 (Seq.length xs)) + intFromList xs

let testValue = 123456
((breakDownInt >> intFromList) testValue) = testValue |> printf "break down / build back test passed: %b\n" 
  
let rec distribute e = function
  | [] -> [[e]]
  | x::xs' as xs -> (e::xs)::[for xs in distribute e xs' -> x::xs]

let rec permute = function
  | [] -> [[]]
  | e::xs -> List.collect (distribute e) (permute xs)
        
// circular, it said, not permutations!
let circular n =
    let rec rotate c howMany = 
        match howMany with
        |0 -> []
        |_ -> 
              let x = (List.tail c) @ [List.head c]
              x :: (rotate x (howMany - 1))
    let l = breakDownInt n
    rotate l (List.length l) |> List.map intFromList
        
let allPrimes permutations =
    permutations
    |> List.map isPrime
    |> List.fold (fun a e -> a && e) true

let primes = seq { for i in 0..500000 do if isPrime (i * 2 + 1) then yield (i * 2 + 1)}

primes 
|> Seq.filter (circular >> allPrimes) 
|> Seq.length
|> printf "Circular primes count: %d\n" // and 1 more, as 2 is not calculated!
