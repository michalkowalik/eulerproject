let isPrime n = 
    match n with
    | x when x < 2 -> false
    | _ -> let bound = int (sqrt(float n))
           seq{2..bound}
           |> Seq.exists (fun x -> n % x = 0) 
           |> not

// infinite sequence of integers:
let positiveIntegers = Seq.unfold (fun x -> Some(x, x + 1)) 0

// boundaries
let max = 999

// formula to calculate:
let f x a b = x * x + a * x + b

// how many primes for current params?
let howManyPrimes (a, b) =
    positiveIntegers
    |> Seq.takeWhile (fun x -> isPrime ( f x a b))
    |> Seq.length 

        
let param = seq { for a in -max..max do
                    for b in -max..max do
                        yield a, b}

let init = Seq.head param, howManyPrimes (Seq.head param)

param
|> Seq.fold (fun (p, v) elem -> if (howManyPrimes elem >= v) then (elem, howManyPrimes elem) else  (p, v))  init
|> fun ((a, b), count) -> printf "Max value for a:%d b:%d is %d\n" a b count
        
        
