 // euler 30
 let sumFifth (n:int) = 
        n.ToString().ToCharArray()
        |> Seq.map (fun x -> System.Int32.Parse(x.ToString()))
        |> Seq.map (fun x -> pown x 5)
        |> Seq.sum
        
seq {2..(6 * (pown 9 5))}
|> Seq.filter (fun x -> sumFifth x = x) 
|> Seq.sum
|> printf "%d\n"