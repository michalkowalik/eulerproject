// euler 31, coins sum
let rec count x coins = 
    match (x, coins) with 
    |(0, _) -> 1
    |(_, []) -> 0
    |(x, c :: cs) -> List.sum [ for i in 0..(x / c) -> count (x - (i * c)) cs ]
    
count 200 [200; 100; 50; 20; 10; 5; 2; 1] |> printf "%d\n"