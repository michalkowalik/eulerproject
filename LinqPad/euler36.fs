// euler problem 36:

// reverse string
let rev (x: string) = new String(Array.rev(x.ToCharArray()))

let isPalindromic (x:int) =
    let binary = Convert.ToString(x,2)
    let decimal = x.ToString()
    decimal = rev decimal && binary = rev binary

seq {1..999999} 
|> Seq.fold (fun acc elem -> if isPalindromic elem then acc + elem else acc) 0 
|> printf "%d\n"
