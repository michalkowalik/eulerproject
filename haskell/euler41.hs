import Data.List

fromDigits = foldl addDigit 0
   where addDigit num d = 10*num + d

isPrime a = isPrimeHelper a primes

isPrimeHelper a (p:ps)
  | p * p > a = True
  | a `mod` p == 0 = False
  | otherwise = isPrimeHelper a ps

primes = 2: filter isPrime [3,5..]

p = filter (\x -> (last x) `mod` 2 == 1) $ permutations [1..7]
m = maximum $ filter isPrime $ map fromDigits p

main:: IO ()
main = do
  print m

