# euler project, problem 40
import itertools

perms = list(itertools.permutations('123456789', 5))


def pandigits(divide):
    for p in perms:
        m = int("".join(p[0:divide]))
        n = int("".join(p[divide:]))
        prod = list(str(m * n))
        # print("m: {}, n: {}, prod: {}".format(m, n, prod))
        if len(list(p) + prod) == 9 and (not '0' in prod) and (len(set(list(p) + prod)) == 9):
            yield (m, n, ''.join(prod))

            
s1 = sum(set([int(a[2]) for a in pandigits(2)]))
s2 = sum(set([int(a[2]) for a in pandigits(1)]))

print(s1 + s2)
