from euler18 import Euler18


def euler18_test():
    e = Euler18("c:/garage/haskell/euler18.txt")
    assert e.solve() == 23
