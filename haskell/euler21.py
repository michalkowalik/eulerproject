import time


class Euler21:

    def divisors_sum(self, n):
        return sum([i for i in range(1, int(n / 2) + 1) if n % i == 0])

    def solve(self):
        print(sum([i for i in range(1, 10001)
                   if i == e.divisors_sum(e.divisors_sum(i)) and i != e.divisors_sum(i)]))

        
## main:
e = Euler21()
start = time.time()
e.solve()
print("Execution took {} sec".format(time.time() - start))
