import Data.List

getProducts :: String -> [Int]
getProducts inp | length inp < 5 = [0]
                | otherwise = getProd (take 5 inp) : getProducts (tail inp)
                              where getProd x = foldl (\a b -> a * (read [b])) 1 x 

main:: IO ()
main = do
  input <- readFile "euler08.txt"
  let res = maximum (getProducts $ foldl (++) "" (lines input))
  print res
