-- problem 37, haskell take 1
import Data.List


sieve (x:xs) = x: sieve( filter ((/= 0) . (`mod` x)) xs)


isPrime a = isPrimeHelper a primes

isPrimeHelper a (p:ps)
  | p * p > a = True
  | a `mod` p == 0 = False
  | otherwise = isPrimeHelper a ps

primes = 2: filter isPrime [3,5..]

