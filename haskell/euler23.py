#! /bin/env python

import os
os.chdir("c:/garage/")
import pickle
import time


def divisors(n):
    return [i for i in range(1, int(n / 2) + 1) if n % i == 0]


def abundant_numbers():
    return [i for i in range(2, 28124) if i < sum(divisors(i))]


def save_numbers():
    pickle.dump(abundant_numbers(), open("c:/garage/haskell/euler22.data", "wb"))

    
def load_numbers():
    return pickle.load(open("c:/garage/haskell/euler22.data", "rb"))


def abundant_sums(ab_numbers):
    return set([i + j for i in d for j in d if i + j < 28124 and j >= i])


##main
start = time.time()
d = load_numbers()
print(len(d))
ab_sums = abundant_sums(d)

print(sum([i for i in range(1, 28124) if not i in ab_sums]))
print("Took {} sec".format(time.time() - start))
