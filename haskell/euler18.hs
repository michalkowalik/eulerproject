
triangle = 
    [[75],
     [95, 64],
     ... Snip ...
     [04, 62, 98, 27, 23, 09, 70, 98, 73, 93, 38, 53, 60, 04, 23]]

main = print (foldr1 reduce triangle)
    where reduce a b = zipWith (+) a (zipWith max b (tail b))
