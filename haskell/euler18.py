import time


class Euler18:
    lines = []

    def __init__(self, path):
        for line in open(path, 'r'):
            self.lines.append([int(i) for i in line.split()])

    ##
    # solve bottom up: (?)
    def solve(self):
        for row in range(len(self.lines) - 2, -1, -1):
            for col in range(row + 1):
                self.lines[row][col] += max([self.lines[row + 1][col], self.lines[row + 1][col + 1]])
        return self.lines[0][0]
    

def main():
    start = time.time()
    e = Euler18('c:/garage/haskell/triangle.txt')
    print(e.solve())
    elapsed = time.time() - start
    print("took {} sec.".format(elapsed))
    
if __name__ == "__main__":
    main()
