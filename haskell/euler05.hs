import Data.List

divisible :: Integral a=> a -> a -> Bool
divisible x maxRange = foldl (\a b -> a + x `mod` b) 0 [1..maxRange] == 0


sieve (x:xs) = x: sieve( filter ((/= 0) . (`mod` x)) xs)


isPrime a = isPrimeHelper a primes

isPrimeHelper a (p:ps)
  | p * p > a = True
  | a `mod` p == 0 = False
  | otherwise = isPrimeHelper a ps

primes = 2: filter isPrime [3,5..]


main :: IO ()
main = do
  let r = take 1 [x | x <- map (*20) [1..], divisible x  20]
  print r

