class Euler19:
    
    def month_days(self, year):
        if year % 4 != 0 or (year % 400 != 0 and year % 100 == 0):
            return [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        else:
            return [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    def solve(self):
        days = 0
        res = []
        for year in range(1900, 2001):
            month = 1
            for d in self.month_days(1900):
                days += d
                month += 1
                if (days + 1) % 7 == 0:
                    res.append((year, month))
        return res


# main:
e = Euler19()
print(e.month_days(2012))
months = e.solve()
print(len(months) - 2)
