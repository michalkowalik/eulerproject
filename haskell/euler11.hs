import Data.List

max_x = 19
max y = 19

prod :: [Int] -> [Int]
prod d | length d < 4 = []
       | otherwise = foldr (*) 1 (take 4 d) : prod (tail d)
                     
maxi d = maximum [maximum $ prod x | x <- d]

-- diagonal:
         -- 1: left to right
dia  d = maxi [[d!!0!!(x), d!!1!!(x+1), d!!2!!(x+2), d!!3!!(x+3)] |x <-[0..15]]
dia' d = maxi [[d!!3!!(x), d!!2!!(x+1), d!!1!!(x+2), d!!0!!(x+3)] |x <-[0..15]]


full_dia x | length x < 4 = []
           | otherwise = dia (take 4 x): full_dia (tail x)

full_dia' x | length x < 4 = []
           | otherwise = dia' (take 4 x): full_dia' (tail x)


main:: IO ()
main = do
  input <- readFile "euler11.txt"
  let d = [map (\a -> read a :: Int) (words x) | x <- (lines input)]
  putStrLn "Max H: "
  print $ maxi d
  let pivoted = [[ d!!i!!j | i <- [0..19]] | j <- [0..19]]
  putStrLn "Max V:"
  print $ maxi pivoted
  putStrLn "Max dia 1:"
  print $ maximum $ full_dia  d
  print $ maximum $ full_dia' d
  
