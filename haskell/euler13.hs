main :: IO ()
main = do
  inp <- readFile "euler13.txt"
  let d = map read (lines inp) :: [Integer]
  print $ take 10 (show $ sum d)
