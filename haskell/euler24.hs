import Data.List
import qualified Data.List.Key as K

-- lexicographic permutations.
perms :: Eq a => [a] -> [[a]]
perms [] = [[]]
perms xs = [x:ys | x <- xs, ys <- perms (delete x xs)]

main :: IO ()
main = print $ (perms [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])!!999999
