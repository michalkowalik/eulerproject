def iter_fibo():
    a, b = 0, 1
 
    while True:
        yield b
        a, b = b, a + b


def main():
    i = 1
    for a in iter_fibo():
        if len(str(a)) >= 1000:
            print(i)
            break
        i += 1

if __name__ == "__main__":
    main()
