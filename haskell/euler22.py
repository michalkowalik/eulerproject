import time


def name_value(name):
    return sum([ord(a) - 64 for a in list(name)[1:-1]])
    
## main:
start = time.time()
f = open('names.txt', 'r')
names = f.read().split(',')
names.sort()

print(sum([(i + 1) * name_value(names[i]) for i in range(0, len(names))]))
print("took {} sec".format(time.time() - start))
