import time


class Euler26:

    def __init__(self):
        pass

    def fraction(self, n):
        rest = 1
        while True:
            div = int((rest * 10) / n)
            rest = (rest * 10) % n
            ##print("d: {} r: {}".format(div, rest))
            yield div
            if rest == 0:
                return ""
    
    def solve(self):
        pass

    
def recurring_cycle(n, d):
    # solve 10^s % d == 10^(s+t) % d
    # where t is length and s is start
    for t in range(1, d):
        if 1 == 10 ** t % d:
            return t
    return 0


def main():
    e = Euler26()
    e.solve()
    a = []
    for i in e.fraction(7):
        a.append(i)
        if len(a) > 50:
            break
    print(''.join([str(z) for z in a]))

if __name__ == "__main__":
    start = time.time()
    main()
    print("excution took {} sec".format(time.time() - start))
    longest = max([recurring_cycle(1, i) for i in range(2, 1001)])
    print([i for i in range(2, 1001) if recurring_cycle(1, i) == longest])
