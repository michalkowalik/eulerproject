import Data.List

helper acc 1 = acc
helper acc n = case n `divMod` 2 of (n', 0) -> helper (acc + 1) n'
                                    _       -> helper (acc + 1) (3 * n + 1)
g x y = if snd x < snd y then y else x
h x n = x  `g` (n, helper 1 n)
                                    
maxCollatz n = fst $ foldl' h (1,1) [2..n-1]
          
main :: IO ()
main = print a1
  where a1 = maxCollatz 999999
