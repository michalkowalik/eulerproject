import Data.List

sieve (x:xs) = x: sieve( filter ((/= 0) . (`mod` x)) xs)


isPrime a = isPrimeHelper a primes

isPrimeHelper a (p:ps)
  | p * p > a = True
  | a `mod` p == 0 = False
  | otherwise = isPrimeHelper a ps

primes = 2: filter isPrime [3,5..]



main :: IO ()
main = do
  print "Euler project, problem 46"
