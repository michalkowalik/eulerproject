-- problem 39, with full blown pythagorean triples:

import Data.List
import Data.Function

pythTripels n = [(k*x, k*y, k*z) | (x,y,z) <- primitives, k <- [1..n`div`z]] where
   primitives = [(p^2-q^2, 2*p*q, p^2+q^2) | p <- takeWhile (\p -> p^2+1 <= n) [1..], q <- takeWhile (\q -> p^2+q^2 <= n) [1..p], odd (p+q) && gcd p q == 1]

triangles = filter (\(a, b, c) -> a + b + c < 1001) $ pythTripels 1000
perimeters = group $ sort $ map (\(a, b, c) -> a + b + c) triangles
res = map (\x -> (head x, length x)) perimeters

main:: IO ()
main = do
  print $ last $ sortBy (compare `on` snd) res
