class Euler:

    numbers = {1: "one",
               2: "two",
               3: "three",
               4: 'four',
               5: 'five',
               6: 'six',
               7: 'seven',
               8: 'eight',
               9: 'nine',
               10: 'ten',
               11: 'eleven',
               12: 'twelve',
               13: 'thirteen',
               14: 'fourteen',
               15: 'fifteen',
               16: 'sixteen',
               17: 'seventeen',
               18: 'eighteen',
               19: 'nineteen',
               20: 'twenty',
               30: 'thirty',
               40: 'forty',
               50: 'fifty',
               60: 'sixty',
               70: 'seventy',
               80: 'eighty',
               90: 'ninety',
               100: 'hundred',
               1000: 'thousand'}

    def __init__(self):
        pass

    def number_in_words(self, n):
        if n == 1000:
            return "one " + self.numbers[n]
        res = ""
        hundreds = int(n / 100)
        tens = int((n % 100) / 10)
        ones = n % 10

        if hundreds > 0:
            res += self.numbers[hundreds] + " " + self.numbers[100]
        if tens > 0:
            if hundreds > 0:
                res += " and "
            if tens == 1:
                res += self.numbers[n % 100]
            else:
                res += self.numbers[tens * 10]
        if ones > 0 and tens != 1:
            if tens > 0:
                res += ' '
            elif hundreds > 0:
                res += ' and '
            res += self.numbers[ones]
        return res

    def solve(self):
        return sum([len(''.join(self.number_in_words(i).split())) for i in range(1, 1001)])


def main():
    print(Euler().solve())

if __name__ == "__main__":
    main()
