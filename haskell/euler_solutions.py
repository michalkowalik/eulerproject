def diagonal_sum(a):
    l = len(a)
    d1 = sum([a[i][i] for i in range(len(a))])
    d2 = sum([a[l - 1 - i][i] for i in range(len(a))])
    print(d1 + d2 - 1)

    
def euler27(n):
    a = [[0] * n for i in range(n)]
    print(a[n - 1][n - 1])
    print(a[0][0])

    ## fill:
    center = (int(n / 2), int(n / 2))
    round = 1
    pos = center
    x = 2
    a[center[0]][center[1]] = 1

    while round <= int(n / 2):
        print("start pos: {}".format(pos))
        # go right:
        while pos[0] < center[0] + round:
            pos = (pos[0] + 1, pos[1])
            a[pos[1]][pos[0]] = x
            x += 1
        # go down:
        while pos[1] < center[1] + round:
            pos = (pos[0], pos[1] + 1)
            a[pos[1]][pos[0]] = x
            x += 1
        # go left:
        while pos[0] > center[0] - round:
            pos = (pos[0] - 1, pos[1])
            a[pos[1]][pos[0]] = x
            x += 1
        # go up:
        while pos[1] > center[1] - round:
            pos = (pos[0], pos[1] - 1)
            a[pos[1]][pos[0]] = x
            x += 1
        # go right:
        while pos[0] < center[0] + round:
            pos = (pos[0] + 1, pos[1])
            a[pos[1]][pos[0]] = x
            x += 1
            
#        for y in a:
#            print(y)
#        print("------")
            
        round += 1

    diagonal_sum(a)
    #for x in a:
    #    print(x)

    
#
euler27(1001)
