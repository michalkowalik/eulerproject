﻿namespace EulerProject

open System.Numerics

module Euler45 = 

    let isPentagonal (x:int64) =
      let delta = 1L + 24L * x
      let x1 = (sqrt(float delta) + 1.0) / 6.0
      x1 = float (int64 x1)

    let euler45 () =
      printf "Euler project, problem 45 \n"
      seq {for x in EulerHelper.positiveIntegers do
             let hex = int64(x * (2 * x - 1))
             if isPentagonal (hex) then yield hex
            }
      |> Seq.take 3 |> Seq.last |> printf "X: %d \n"
