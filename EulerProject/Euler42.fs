﻿namespace EulerProject

module Euler42 = 
    
    // Value to remove from char representation of letter:
    let a = 64

    //words
    let wordsFile = @"e:\garage\bucket\EulerProject\words.txt"
    let data = 
        System.IO.File.ReadAllText(wordsFile).Split [|','|]
        |> List.ofArray
        |> List.map (fun s -> s.Replace("\"", ""))
    
    // triangle numbers - the longest word in file is 14 char long
    // 14 * 26 (Z) = 364, hence upper bound
    let triangleNumbers = 
        EulerHelper.positiveIntegers
        |> Seq.map (fun i -> (i * (i + 1)) / 2 )
        |> Seq.takeWhile (fun x -> x < 365)
        |> List.ofSeq

    let wordToNumber (s:string) =
        s 
        |> Seq.map (fun x -> (int x) - a) 
        |> Seq.sum 


    let euler42 () = 
        printf "Euler project, problem 42\n"
        data
        |> List.map wordToNumber
        |> List.filter (fun w -> List.exists (fun x -> x = w) triangleNumbers)
        |> List.length
        |> printf "Found %d triangle words\n"
