﻿namespace EulerProject

module Euler30 =
            
    // 999999 as an upper limit of what is technically possible:
    // 999999 is bigger than 6 * 9^5

    let sumFifth (n:int) = 
        n.ToString().ToCharArray()
        |> Seq.map (fun x -> System.Int32.Parse(x.ToString()))
        |> Seq.map (fun x -> pown x 5)
        |> Seq.sum

