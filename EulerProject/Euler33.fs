﻿namespace EulerProject

module Euler33  =

    let cancelDigits enumerator denominator = 
     //   printf "Checking %d/%d\n" enumerator denominator
        let x =seq { for d in EulerHelper.breakDownInt denominator |> Seq.filter (fun x -> x <> 0)  do
                         for e in EulerHelper.breakDownInt enumerator do
                             yield (e, d)
                   }
                   |> Seq.filter (fun (e, d) -> (float e) / (float d) = (float enumerator) / float(denominator))
                   |> Seq.filter (fun (e, d) -> (float e) / (float d)  <> 1.00)
                   |> Seq.distinct
        not (Seq.isEmpty x)

        

    let euler33 () = 
        printf "Euler project problem 33 \n"

        seq { for denominator in 11..99 do
                  for enumerator in 10..denominator do
                      yield (enumerator, denominator) }
        // remove obvious:
        |> Seq.filter (fun (e, d) -> (e % 10 <> 0) && (d % 10 <> 0))
        |> Seq.filter (fun (e, d) -> (e % 11 <> 0) && (d % 11 <> 0))
        
        |> Seq.filter (fun (e, d) -> cancelDigits e d)
        |> Seq.iter (fun (e, d) -> printf "%d/%d \n" e d)