﻿namespace EulerProject

module Euler49 = 
    
    let primes = EulerHelper.primes
                |> Seq.takeWhile (fun x -> x < 10000)
                |> Seq.filter (fun x -> x > 999)
                |> List.ofSeq

    // find permutations of the prime:
    let permutationsOf n =
        let dn = n |> EulerHelper.breakDownInt |> List.sort
        primes |> List.filter (fun x -> (x |> EulerHelper.breakDownInt |> List.sort) = dn)

    let rec permutationList p acc = 
        let alreadyFound p =
            acc |> List.fold (fun a el -> (List.exists (fun x -> x = p) el) || a) false

        match p with
        | [] -> acc
        | x :: xs -> if alreadyFound x then permutationList xs acc
                     else permutationList xs <| permutationsOf x :: acc

    let permutations = permutationList primes [] |> List.filter (fun x -> List.length x > 2)

    // find sequence in a list:
    let differences l = List.tail l |> List.map (fun x -> x - List.head l)

    let rec sequences l acc =
        match l with
        | [] -> acc
        | x::xs -> sequences xs ((differences l) :: acc)

    let findPermutations l = 
        let h = l |> List.head 
        let t = l |> List.tail |> Array.ofList

        [0..(List.length h) - 1]
        |> List.map (fun x ->
                     if (List.exists (fun y -> y = h.[x]) t.[x])
                     then (x, h.[x], t.[x])
                     else (0, 0, []))
        |> List.filter (fun (a, b, c) -> b <> 0)

    let singleSolver e =
        let rec helper d =
          if List.isEmpty d then []
          else
            let s = findPermutations d |> List.filter (fun (a, b, c) -> b <> 0)
            if not (List.isEmpty s) then
              s
            else helper (List.tail d)
            
        helper (sequences e [] |> List.rev)

    let euler49 () =
        printf "Euler project, problem 49 \n"
        permutations
        |> List.map singleSolver
