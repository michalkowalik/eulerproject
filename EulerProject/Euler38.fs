﻿namespace EulerProject

module Euler38 = 
    open Microsoft.FSharp.Collections

    // generate series of concatenated products:
    let cp (n:int) = 
        let rec products (acc:List<int>) (counter:int) = 
            if (List.map (string >> String.length) acc |> List.sum) < 9 then
                products ((n * counter) :: acc) (counter + 1)
            else
                acc

        products [] 1

    let isPandigital n = 
        let s =
            cp n
            |> List.map string
            |> List.fold (+) ""
        
        (s.Length = 9) && (not (s.Contains "0")) && (s|> Seq.distinct |> Seq.length) = 9
        
    let euler38 () = 
        printf "Euler project, problem 38\n"

        let biggest  =
            EulerHelper.positiveIntegers
            |> Seq.take 10000
            |> Seq.filter isPandigital
            |> Seq.last

        cp biggest
        |> List.rev
        |> Seq.map string
        |> Seq.fold (+) ""
        |> printf "biggest: %s\n"
