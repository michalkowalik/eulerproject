namespace EulerProject

module Euler44 =

  // formula for pentagon number 
  let pent n = n * (3 * n  - 1) / 2

  // let's start with 500 numbers to work on:
  let data = [1..5000] |> List.map pent |> Set.ofList


  let euler44 () =
    printf "euler project, problem 44 \n"
 
    let ppairs = [| for x in data do
                      for y in data do
                        if (data.Contains (x + y) && data.Contains (x - y)) then yield (abs (y - x))|]
    ppairs
    |> Array.min
    |> printf "Min diff: %d \n"
    
    
