﻿namespace EulerProject

module Euler46 =
    
    // helper function - check if a number is a square of ints:
    let isSquareOfInt n =
        let x = sqrt (float n)
        x = float(int64 x)

    let mutable (pp:array<int64>) = [||]

    let goldbach (n:int64) =
        if EulerHelper.isPrimeLong n then
            pp <- Array.append pp [|n|]
            true
        else
            let squares = pp |> Array.filter (fun p -> p < n)
                             |> Array.map (fun p -> (n - p) /2L)
            let r = seq {for x in squares do
                            if isSquareOfInt x then
                                yield x
                        }
            not <| Seq.isEmpty r
            

    // and function to handle it:
    let solver () =
        seq { for x in EulerHelper.positiveIntegersLong do yield (x * 2L) + 1L }
        |> Seq.filter (goldbach >> not) 
        |> Seq.take 1
        |> Seq.head |> printf "Res: %d \n"


    let euler46 () =
        printf "EulerProject, problem 46 \n"
        solver ()
