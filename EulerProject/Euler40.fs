﻿namespace EulerProject

module Euler40 = 

    // I need 1e6 elements at least
    // 200 000 is the limit

    let champertowne = 
        Seq.fold (fun acc x ->  acc  + (x |> string)) "" (seq {1..200000})

    let euler40 () = 
        printf "Euler Project, problem 40 \n"

        champertowne |> Seq.length |> printf "champertowne length: %d \n"

        printf "1: %c, 10: %c, 100: %c,  1000: %c, 10000: %c, 100000: %c, 1000000: %c \n" champertowne.[0] champertowne.[9] champertowne.[99] champertowne.[9999] champertowne.[99999] champertowne.[999999]