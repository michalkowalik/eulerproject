﻿namespace EulerProject

module Euler37 = 
    
    let truncable prime =
        let rec isTruncable s = 
            match s with 
            | [] -> true
            | x :: xs -> (EulerHelper.isPrime (EulerHelper.intFromList(s))) && (isTruncable xs)

        let rec isRightTruncable s =
            let r = List.rev s
            match s with
            | [] -> true
            | x :: xs -> EulerHelper.isPrime(EulerHelper.intFromList(r)) && (isRightTruncable xs)

        let s = prime |> EulerHelper.breakDownInt
        isTruncable s  && (isRightTruncable (List.rev s))

    let euler37 () = 
        printf "EulerProject, problem 37\n"

        EulerHelper.primes
        |> Seq.filter (fun x -> x > 10)
        |> Seq.filter truncable
        |> Seq.take 11
        |> Seq.sum
        |> printf "%d\n"