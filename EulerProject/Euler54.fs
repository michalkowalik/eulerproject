﻿namespace EulerProject

module Euler54 =

    type Suit = 
    | Club| Diamond| Heart| Spade

    type Card = Rank of int * Suit

    // get  value of single card:
    let parseCard (s:string) =
        let rank = match s.[0] with
                    | '2' -> 2
                    | '3' -> 3
                    | '4' -> 4
                    | '5' -> 5
                    | '6' -> 6
                    | '7' -> 7
                    | '8' -> 8
                    | '9' -> 9
                    | 'T' -> 10
                    | 'J' -> 11
                    | 'Q' -> 12
                    | 'K' -> 13
                    | _ -> 14

        let suit = match s.[1] with
                    | 'C' -> Club
                    | 'D' -> Diamond
                    | 'H' -> Heart
                    | _ -> Spade
        (rank, suit)

    let parseCards (cards:string) = 
        cards.Split [|' '|]
        |> Array.toSeq
        |> Seq.map (parseCard)

    // calculate the hand power.
    // high card: return the rank of the highest card. Max: 14
    // pair: 20 + value of pair -> so max: 20 + 28 = 48
    // 2 pairs: 50 + value of both pairs: max: 50 + 28 + 24 = 102
    // three of a kind = 110 + value of the triple. MAX: 110 + 3 * 14 = 152
    // straigth: 160 + 14 + 13 + 12 + 11 + 10 = 220
    // flush : 220 + 14 + 13 + 12 + 11 + 10 = 280
    // full house: 280 + 3 * 14 + 2 * 13 = 348
    // four of a kind: 350 + 4 * 14 = 406
    // straigth flush: 410 + 13 + 12 + 11 + 10 + 9 = 465
    // royal flush: 470 + 14 + 13 + 12 + 11 + 10 = 530

    // power of hand without repeated cards:
    let powerNoRepeats hand = 
        let sorted = hand |> Seq.sortBy fst |> Seq.toList
        let suits = hand |> Seq.map snd |> Seq.distinct
        
        // check if the cards are consecutive - need to check on sorted hand!
        let consec = (fst sorted.[0]) + 4 = fst sorted.[4]
        
        // single suit in hand:
        if Seq.length suits = 1 then
            // flush
            if not consec then
                (sorted |> List.sumBy fst) + 220
            else
                // royal flush
                if (fst sorted.[0] = 10) then
                    530
                // straigth flush
                else 
                    (sorted |> List.sumBy fst) + 410
        else
           // straight
           if consec then 
                (sorted |> List.sumBy fst) + 160
           // high card 
           else
                fst sorted.[4]

    let powerGroups sorted =
        // helper function: calculate values in sorted and grouped hand
        let p s = Seq.fold (fun acc (_, sd) -> acc + (sd |> Seq.map fst |> Seq.sum)) 0 s
        // filter out single cards:
        let s = sorted |> Seq.filter (fun (_, s) -> Seq.length s > 1)

        if Seq.length s = 1 then
            let tmp = Seq.head s
            // 1 group with 4 cards
            // 1 group with 3 cards
            // 1 group with 2 cards
            match (snd tmp |> Seq.length) with
                | 2 -> 20 + 2 * (fst tmp)
                | 3 -> 110 + 3 * (fst tmp)
                | _ -> 350 + 4 * (fst tmp)
        else
            // possible outcomes:
            // 2 pairs
            // full house
            match (s |> Seq.map (fun (_, sd) ->  sd |> Seq.length) |> Seq.toList) with
                | [2;2] -> 50 + p s
                | _ -> 280 + p s


    let power hand = 
        let sorted = hand   
                    |> Seq.sortBy fst
                    |> Seq.groupBy fst
        // no pairs, triples ect. possible outocmes: high, straigth, flush, straigth flush, royal flush:
        if (Seq.length sorted) = 5 then
            powerNoRepeats hand
        // pairs, triple, four of a kind, full house
        else
            powerGroups sorted

    // imported poker hands
    let data = 
        System.IO.File.ReadLines(@"F:/garage/bucket/EulerProject/p54_poker.txt")
        |> Seq.map parseCards
        |> Seq.toList

    let rec evaluateHands hands =
        // helper function to decide which hand (1'st or 2'nd is stronger)
        let compare x =
            let h1 = x |> Seq.take 5 |> power
            let h2 = x |> Seq.skip 5 |> power

            match(h1, h2) with
            | (h1, h2) when h1 > h2 -> 1
            | _ -> 2

        match hands with
        | [] -> []
        | x :: xs -> (compare x) :: evaluateHands xs

    let euler54() = 
        printf "Euler Project, problem 54\n"
        let solution = data 
                       |> evaluateHands
        solution
        |> List.filter (fun(x) -> x = 1)
        |> List.length
        |> printf "Player 1 won %d times \n"
