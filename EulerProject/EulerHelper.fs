﻿namespace EulerProject

module EulerHelper = 

    let isPrimeLong n = 
        match n with
        | x when x < 2L -> false
        | _ -> let bound = int64 (sqrt(float n))
               seq{2L..bound}
               |> Seq.exists (fun x -> n % x = 0L) 
               |> not

    let isPrime n = 
        match n with
        | x when x < 2 -> false
        | _ -> let bound = int (sqrt(float n))
               seq{2..bound}
               |> Seq.exists (fun x -> n % x = 0) 
               |> not



    // infinite sequence of integers:
    let positiveIntegers = Seq.unfold (fun x -> Some(x, x + 1)) 1

    let positiveIntegersLong = Seq.unfold (fun x -> Some(x, x + 1L)) 1L

    // simple sieve implementation:
    let primes = 
        positiveIntegers
        |> Seq.filter isPrime

    let primesLong =
        positiveIntegersLong
        |> Seq.filter isPrimeLong

    // convert int to a list of digits, keep the order
    let breakDownInt n =
        n
        |> string
        |> Seq.map (string >> int)
        |> Seq.toList

    // build integer out of the list of digits
    let rec intFromList s = 
        match s with 
        | [] -> 0
        | x :: xs -> x * (pown 10 (Seq.length xs)) + intFromList xs

    // tail recursion optimised factorial function
    let factorial x =
        let rec tailRecursiveFactorial x acc =
            if x <= 1 then 
                acc
            else 
                tailRecursiveFactorial (x - 1) (acc * x)

        tailRecursiveFactorial x 1

    // permutations:
    let rec insertions x = function
        | []             -> [[x]]
        | (y :: ys) as l -> (x::l)::(List.map (fun x -> y::x) (insertions x ys))
 
    let rec permutations = function
        | []      -> seq [ [] ]
        | x :: xs -> Seq.concat (Seq.map (insertions x) (permutations xs))
