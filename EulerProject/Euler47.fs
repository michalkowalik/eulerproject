﻿namespace EulerProject

module Euler47 = 
    
    let primes = EulerProject.EulerHelper.primes |> Seq.takeWhile (fun z -> z < 1000000) |> Set.ofSeq

    let isPrime y =
        primes |> Set.exists ((=) y)


    // find first prime smaller or equal to n:
    let rec fstSmallerPrime n =
        if EulerProject.EulerHelper.isPrime n then n
        else fstSmallerPrime(n - 1)
//        match n with
//            | i when i % 2 = 0 ->
//                fstSmallerPrime (n - 1)
//            | i when EulerProject.EulerHelper.isPrime i ->
//                i
//            | _ -> fstSmallerPrime (n - 1)

    // find first prime factor:
    // n: number to be factored:
    // x: prime:
    let rec fstPrimeFactor n x =
        if (n % x) = 0 then x
        else fstPrimeFactor n (fstSmallerPrime (x - 1))

    // let's get a list of prime factors of a number:
    // return a list of prime factors or an empty list:
    let rec primeFactors n =
        if EulerProject.EulerHelper.isPrime(n) then [n]
        elif n < 2 then []
        else 
            let fstPrime = fstSmallerPrime (n / 2)
            let fstFactor = fstPrimeFactor n fstPrime
            fstFactor :: primeFactors(n / fstFactor)

    // hack - use only for rather biggish numbers!
    let bigPrimeFactors n = 
        if EulerProject.EulerHelper.isPrime(n) then [n]
        elif n < 2 then []
        else 
           // let fstPrime = fstSmallerPrime (n / 2)
           // that won't work for every possible number, but should be fine for big enough..
            let fstPrime = fstSmallerPrime (n / 30)
            let fstFactor = fstPrimeFactor n fstPrime
            fstFactor :: primeFactors(n / fstFactor)


    // filter the n (list of ints), add to acc consecutive numbers with 4 unique factors
    let rec consecutive acc n =
        printf "Checking %d, current list length: %d \n" n (acc |> List.length)
        if (List.length acc) = 4 then acc
        else
            let factors = bigPrimeFactors n |> Seq.distinct |> Seq.length
            match factors with
                | 4 -> consecutive (n :: acc) (n + 1)
                | _ -> consecutive [] (n + 1)


    let euler47 () = 
        let a = consecutive [] 61000

        printf "Euler Project, problem 47, first number is: %d \n" (a |> Seq.head)
