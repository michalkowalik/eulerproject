﻿namespace EulerProject

module Euler41 = 

    let isPandigital n = 
        let s =
            n
            |> List.map string
            |> List.fold (+) ""
        
        (s.Length = 9) && (not (s.Contains "0")) && (s|> Seq.distinct |> Seq.length) = 9


    let max = 987654322 // quick and dirty, no idea if there's pandigital prime of a length 9:

    let p = EulerHelper.primes
            |> Seq.takeWhile (fun x -> x < max)
            |> Seq.map EulerHelper.breakDownInt
            |> Seq.filter isPandigital

    let euler41() = 
        printf "Euler project, problem 41 \n"