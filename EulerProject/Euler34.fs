﻿namespace EulerProject

open System

module Euler34 =


//    TRY with unfold!
//    let rec decompose2 (n:int) =
//        if n = 0 then
//            [0]
//        else
//            let maxLog = Math.Log10 (n |> float) |> int
//
//            let rec d number l acc=
//                match l with
//                | 0 -> acc @ number
//                | _ -> 
//                    let digit = number / (pown 10 l)
//                    
//
//            d n maxLog []

    let sumDigitFac l = 
        l
        |> Seq.map EulerHelper.factorial
        |> Seq.sum

    let isFactorialSum n =
        (EulerHelper.breakDownInt >> sumDigitFac) n = n

    let euler34 () = 
        printf "Euler project, problem 34\n"

        seq {11..9999999}
        |> Seq.filter isFactorialSum
        |> Seq.iter (printf "%d \n")


