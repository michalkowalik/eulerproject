﻿namespace EulerProject

module Euler48 = 
    
    let numberLength = 10000000000L

    // return last 10 digits of the n to the power of n:
    let selfPower n = 
        [for i in 0..(n - 1) -> n]
        |> List.fold (fun (acc:int64) el -> (acc * int64(el)) % numberLength) 1L

    let euler48 () = 
        (List.sum [for i in 1..1000 -> selfPower i]) % numberLength
        |> printf "Last 10 digits of the sum: %d \n"
