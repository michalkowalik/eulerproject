﻿namespace EulerProject

module Euler81 = 
    // discriminated union to keep possible move directions:
    type Move = 
        | Down
        | Right
    
    // data file
    let matrixData = @"e:\garage\bucket\EulerProject\p081_matrix.txt"
    let maxVal = 79
    
    let data = 
        System.IO.File.ReadLines(matrixData)
        |> Seq.map (fun line -> 
               line.Split [| ',' |]
               |> Array.map int
               |> Array.toList)
        |> Seq.toList
    
    let dataValueAt (y, x) = data.[y].[x]
    
    // get next move(s) from current position:
    let nextMoves ((py, px), pathSum, moveLog) = 
        let move direction = 
            let point = 
                match direction with
                | Down -> (py + 1, px)
                | Right -> (py, px + 1)
            (point, pathSum + (dataValueAt point), direction :: moveLog)
        match (py, px) with
        | (79, 79) -> [ ((py, px), pathSum, moveLog) ]
        | (79, _) -> [ move Right ]
        | (_, 79) -> [ move Down ]
        | (_, _) -> 
            [ (move Right)
              (move Down) ]
    
    let isAtGoal paths = 
        let ((a, b), s, l) = List.head paths
        a = maxVal && b = maxVal
    
    let reduceDups paths = 
        let minPath x = x |> Seq.minBy (fun (p, s, l) -> s)
        paths
        |> Seq.groupBy (fun (p, s, l) -> p)
        |> Seq.map (fun (p, l) -> minPath l)
        |> List.ofSeq
    
    let rec allPaths paths = 
        if (isAtGoal paths) then paths
        else 
            paths
            |> List.map nextMoves
            |> List.concat
            |> reduceDups
            |> allPaths
    
    let euler81() = 
        printf "Euler project, problem 81 \n"
        let paths = allPaths ([ (0, 0), dataValueAt (0, 0), [] ])
        paths
        |> List.map (fun (p, s, l) -> s)
        |> List.min
        |> printf "Min path value: %d \n"
