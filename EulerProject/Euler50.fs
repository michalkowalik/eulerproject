﻿namespace EulerProject

module Euler50 =
    
    let primes = EulerHelper.primes |> Seq.takeWhile (fun x -> x < 1000000) |> List.ofSeq

    let toCheck = List.filter (fun x -> x > 1000) primes

    let consecutiveSum n = 
        let rec helper p seqLen seqSum =
            match seqSum with 
            | x when x = n -> seqLen
            | x when x < n -> helper (List.tail p) (seqLen + 1) (seqSum + List.head p)
            | _ -> 0
        
        // list with primes:
        let rec find p =
            let res = helper p 0 0
            if res > 0 then
                res
            else
                if (p |> List.tail |> List.head) = n then
                    0
                else
                    find (List.tail p)
                                
        find primes

    let euler50 () =
        printf "Euler Project, problem 50 \n"
        let m = toCheck
                |> List.map (fun x -> (x, consecutiveSum x))
                |> List.maxBy (fun (x, l) -> l)
        printf "Max number: %d, length: %d \n" (fst m) (snd m)