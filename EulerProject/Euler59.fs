﻿namespace EulerHelper

module Euler59 =
    let dataFile = @"e:\garage\bucket\EulerProject\EulerProject\cipher.txt"

    let data = 
        System.IO.File.ReadAllText(dataFile).Split(',')
        |> Array.map int


    let euler59 () = 
        printf "Euler project. problem 59\n"

        // let a1 = data |> Array.filter (fun x -> x % 3 = 0)
        // let a2 = data |> Array.filter (fun x -> x % 3 = 1);;
        // let a3 = data |> Array.filter (fun x -> x % 3 = 2);;

        // cipher: search for most common char in earch group, chance is, it's a space:
        // a3 |> Array.groupBy id |> Array.map (fun (a,b) -> (a, Array.length b)) |> Array.sortBy (fun (a,b) -> b);;

        //cipher: god: 
        let cipher = [|103; 111; 100|]

        [0..(Array.length data) - 1]
        |> List.iter (fun x -> match (x % 3) with
                               | 0 -> data.[x]  <- data.[x] ^^^ cipher.[0]
                               | 1 -> data.[x]  <- data.[x] ^^^ cipher.[1]
                               | _ -> data.[x]  <- data.[x] ^^^ cipher.[2]
                    )
        let decoded = data 
        
        decoded 
        |> Array.map char
        |> List.ofArray
        |> List.fold (fun str x -> str + x.ToString()) ""
        |> printf "%s \n"

        Array.sum decoded
        |> printf "Sum: %d \n"
