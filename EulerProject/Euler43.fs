﻿namespace EulerProject

open System.Numerics

module Euler43 =
    
    let panNumbers = 
        EulerHelper.permutations ['0'..'9']
        |> Seq.map (Seq.toArray >> System.String)
        |> Seq.filter (fun x -> x.[0] <> '0')

    let primes = [2; 3; 5; 7; 11; 13; 17]

    let rec solve (acc:seq<string>) i = 
        match i with
        | 7 -> acc
        | _ -> solve (acc |> Seq.filter (fun x -> (x.[(i + 1)..(i + 3)] |> int) % primes.[i] = 0)) (i + 1)

    let euler43 () =
        printf "Euler project, problem 43 \n"
        solve panNumbers 0
        |> Seq.map BigInteger.Parse
        |> Seq.sum
        |> printf "SUM: %A \n"